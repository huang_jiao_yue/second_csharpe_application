﻿using System;

namespace 第二次CSsharpe
{
    class Program
    {
        

        static void Main(string[] args)
        {
            Random rd = new Random();

            //定义常量
            Console.WriteLine("定义常量");
            const double PI = 3.141592653589;//自定义的 派
            //temp = rd.Next() % 100 + 1; //产生随机数在1～100之间
            int b = rd.Next() % 9 + 1;//对边生成0-9之间的数字  头尾可取
            Console.WriteLine("b的值为"+b);
            const int K = 2;//直线斜率
            Console.WriteLine();
            Console.WriteLine("y="+K+"X"+"+"+b);

            int r = 6;//半径
            Console.WriteLine();
            Console.WriteLine( "圆的面积为"+" "+PI*r*r);
            Console.WriteLine();
            Console.WriteLine("圆的周长为" + " " + PI * r *2);

            //循环
            Console.WriteLine();
            Console.WriteLine("循环");
            for (int i=0;i<=6;i++)
            {
                Console.WriteLine(i);
            }

            //条件判断
            Console.WriteLine();
            Console.WriteLine("条件判断");
            int num = rd.Next()%10+1;//随机生成0-10的数字
            var c = 2 >num;
            Console.WriteLine();
            Console.WriteLine("num为"+num);

            //最简单的判断语句   一个分支
            if (c)
            {
                Console.WriteLine("牛啊，条件为真");
            }
            //两个分支的判断语句
            if (c){
                Console.WriteLine();
                Console.WriteLine("它<2");
            }
            else
            {
                Console.WriteLine("它>2");
            }
            //多分支
            if (num==5)
            {
                Console.WriteLine("它是5");
            }
            else if(num == 4)
            {
                Console.WriteLine("它是4");
            }
            else if (num < 3)
            {
                Console.WriteLine("它小于3");
            }
            else
            {
                Console.WriteLine("它大于5");
            } 




            //switch
            Console.WriteLine();
            Console.WriteLine("switch");
            int points = rd.Next() % 100 + 1;
            Console.WriteLine("随机生成学生成绩(0-100)"+"  "+points);
            Console.WriteLine();
            // int points = int.Parse(Console.ReadLine()); 手动输入学生成绩
            switch (points/10)
            {
                case 10:
                    Console.WriteLine("牛啊");
                    break;
                case 9:
                    Console.WriteLine("优秀");
                    break;
                case 8:
                    Console.WriteLine("良好");
                    break;
                case 7:
                    Console.WriteLine("中等");
                    break;
                case 6:
                default:
                    Console.WriteLine("不及格");
                    break;
            }
            Console.WriteLine();


        }

    }
}
