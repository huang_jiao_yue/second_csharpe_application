﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{

    //如果成绩在90分以上是优秀；如果成绩为80~90分是良好；如果成绩为60~90分为及格。如果成绩在60分以下为不及格。
    class Program
    {
        static void Main(string[] args)
        {

            Console.WriteLine("请输入学生考试的成绩(0~100)");
            int points = int.Parse(Console.ReadLine());
            switch (points / 10)
            {
                case 10:
                    Console.WriteLine("优秀");
                    break;
                case 9:
                    Console.WriteLine("优秀");
                    break;
                case 8:
                    Console.WriteLine("良好");
                    break;
                case 7:
                    Console.WriteLine("及格");
                    break;
                case 6:
                    Console.WriteLine("及格");
                    break;
                default:
                    Console.WriteLine("不及格");
                    break;
            }
        }
    }
}

