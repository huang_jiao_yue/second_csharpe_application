﻿using System;

namespace ConsoleApp2
{
    class Program
    {
        static void Main(string[] args)
        {
            const double PI = 3.1415926;

            double r = 12;

            double area = PI * r * r;

            Console.WriteLine(area);
        }
    }
}
    