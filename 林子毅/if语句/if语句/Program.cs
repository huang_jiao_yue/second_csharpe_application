﻿using System;

namespace if语句
{
    class Program
    {
        static void Main(string[] args)
        {
            {
                int b = int.Parse(Console.ReadLine());
                if (b % 4 == 0 && b % 100 != 0)
                {
                    Console.WriteLine("是闰年");
                }
                else if (b % 400 == 0)
                {
                    Console.WriteLine("是闰年");
                }
                else
                    Console.WriteLine("不是闰年");
                Console.ReadLine();
            }
        }
    }
}
