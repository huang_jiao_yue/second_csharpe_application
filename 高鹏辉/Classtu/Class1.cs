﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Classtu
{
    public class Class1
    {
        static void Main(string[] args)
        {
            /*定义常量*/
            const int a = 1;
            const int b = 2;
            const string name = "帅就完事了";
            /*定义变量*/
            int w = 520520520;
            string nice = "一班";
            string nice2 = "二班";
            /*if判断*/
            Console.WriteLine("请输入一班人数");
            int count = int.Parse(Console.ReadLine());
            Console.WriteLine("请输入二班人数");
            int count2 = int.Parse(Console.ReadLine());
            if (count == 42)
            {
                Console.WriteLine("是" + nice);
            }
            else if (count == 43)
            {
                Console.WriteLine("是" + nice2);
            }
            else 
            {
                Console.WriteLine("你输入的不是一班和二班的人数");
            }
            Console.WriteLine("请输入成绩：");
            int cj = int.Parse(Console.ReadLine());
            if (cj < 0 && cj > 100)
            {
                Console.WriteLine("请输入一个0到100的成绩");
            }else
            {
                int n = cj / 10;
                switch (n)
                {
                    case 10:
                        Console.WriteLine("优秀");
                        break;
                    case 9:
                        Console.WriteLine("优秀");
                        break;
                    case 8:
                        Console.WriteLine("良好");
                        break;
                    case 7:
                        Console.WriteLine("及格");
                        break;
                    case 6:
                        Console.WriteLine("及格");
                        break;
                    default:
                        Console.WriteLine("不及格");
                        break;
                }
            }
        }
    }
}
