﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp2
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("请输入一个数字判断是否为偶数");
            int a = int.Parse(Console.ReadLine());
            if (a % 2==0)
            {
                Console.WriteLine("是偶数");
            }
            else
            {
                Console.WriteLine("不是偶数");
            }


            Console.WriteLine("请输入学生成绩(0-100)");
            int b = int.Parse(Console.ReadLine());
            switch (b / 10)
            {
                case 10:
                case 9:
                    Console.WriteLine("优秀");
                    break;
                case 8:
                    Console.WriteLine("良好");
                    break;
                case 7:
                case 6:
                    Console.WriteLine("合格");
                    break;
                default:
                    Console.WriteLine("不合格");
                    break;
            }

            Console.WriteLine("请输入游戏分数(100-1000)");
            int c = int.Parse(Console.ReadLine());
            switch (c / 100)
            {
                case 10:
                    Console.WriteLine("王者");
                    break;
                case 9:
                    Console.WriteLine("大师");
                    break ;
                case 8:
                    Console.WriteLine("钻石");
                    break;
                case 7:
                    Console.WriteLine("铂金");
                    break;
                case 6:
                    Console.WriteLine("黄金");
                    break;
                case 5:
                    Console.WriteLine("白银");
                    break;
                default:
                    Console.WriteLine("青铜");
                    break;
                  

            }
        }
    }
}
