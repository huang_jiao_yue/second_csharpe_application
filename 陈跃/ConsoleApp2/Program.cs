﻿using System;

namespace ConsoleApp2
{
    class Program
    {
        static void Main(string[] args)
        {

            const double PI = 3.14;
            int r = 4;
            Console.WriteLine("圆的周长："+2*PI*r);
            Console.WriteLine("圆的面积："+r*r*PI);


            Console.WriteLine("请输入月份判断是哪一季度");
            int month = int.Parse(Console.ReadLine());
            if (month < 0)
            {
                month = 0;
            }
            if (month <=3)
            {
                Console.WriteLine("第一季度");
            }
            else if (month <=6)
            {
                Console.WriteLine("第二季度");
            }
            else if (month <=9)
            {
                Console.WriteLine("第三季度");
            }
            else if(month <=12 )
            {
                Console.WriteLine("第四季度");
            }
            else
            {
                Console.WriteLine("请输入小等于12的整数");
            }

            Console.WriteLine("请输入本月的工资");
            int money = int.Parse(Console.ReadLine());
            switch (money / 1000)
            {
                case 10:
                case 9:
                case 8:
                    Console.WriteLine("大吃大喝");
                    break;
                case 7:
                case 6:
                case 5:
                case 4:
                    Console.WriteLine("还算凑合");
                    break;
                default:
                    Console.WriteLine("该吃土了");
                    break;
            }

        }
    }
}
