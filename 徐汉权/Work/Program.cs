﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Work
{
    class Program
    {
        static void Main(string[] args)
        {
            //1~10的和
            int Num = 0;
            for (int i = 1; i <= 10; i++)
            {
                Num += i;
            }
            Console.WriteLine("1~10的和为：" + Num);

            //打印九九乘法表
            var Str = "";
            for (int i = 1; i < 10; i++)
            {
                for (int j = 1; j <= i; j++)
                {
                    Str += "" + j + "x" + i + "=" + i * j + "\t";
                }
                Str += "\n";
            }
            Console.WriteLine(Str);

            //If else 语句
            Console.WriteLine("请输入一个整数:");
            int Sun = int.Parse(Console.ReadLine());
            if (Sun % 2 == 0)
            {
                Console.WriteLine("是整数");
            }
              else if (Sun % 2 == 1)
            {
                Console.WriteLine("不是整数");
            }

            //Switch 语句
            Console.WriteLine("欢迎来到徐右右水果店，请问您想先咨询哪一种水果的价格，有一些水果因为疫情原因暂时紧缺，敬请见谅！");
            string Fiuits = Console.ReadLine();
            Console.WriteLine(Fiuits);
            switch (Fiuits)
            {
                case "苹果":
                    Console.WriteLine("苹果：8元/斤");
                    break;
                case "香蕉":
                    Console.WriteLine("香蕉：6元/斤");
                    break;
                case "香梨":
                    Console.WriteLine("香梨：12元/斤");
                    break;
                case "草莓":
                    Console.WriteLine("草莓：15元/斤");
                    break;
                case "黑提":
                    Console.WriteLine("黑提：13元/斤");
                    break;
                default :
                    Console.WriteLine("抱歉！因为疫情原因，其他水果暂时紧缺，还请见谅！");
                    break;
            }
        }
    }
}
