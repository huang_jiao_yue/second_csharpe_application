﻿using System;

namespace ConsoleApp1
{
    class Program
    {
        static void Main(string[] args)
        {
            //常量与变量

            const int r = 4;

            double S = Math.PI * r * r;

            Console.WriteLine(S);

            //if else语句
            const int grade = 90;//定义常量成绩

            if (grade >= 80 && grade < 100)//判断是否优秀
            {
                Console.WriteLine("成绩优秀");
            }
            else if (grade >= 60 && grade < 80)//判断是否良好
            {
                Console.WriteLine("成绩良好");
            }
            else
            {
                Console.WriteLine("不及格");
            }
        //  switch

        //判断输入的是星期几
        init://标记一个位置
            Console.WriteLine("请输入1-7判断是星期几");
            int week = Convert.ToInt32(Console.ReadLine());//将输入的转换为整数

            switch (week)
            {
                case 7:
                    Console.WriteLine("今天是星期日");
                    break;
                case 6:
                    Console.WriteLine("今天是星期六");
                    break;
                case 5:
                    Console.WriteLine("今天是星期五");
                    break;
                case 4:
                    Console.WriteLine("今天是星期四");
                    break;
                case 3:
                    Console.WriteLine("今天是星期三");
                    break;
                case 2:
                    Console.WriteLine("今天是星期二");
                    break;
                case 1:
                    Console.WriteLine("今天是星期一");
                    break;
                default:
                    Console.WriteLine("输入不正确，请重新输入");
                    goto init; //跳至标签重新输入
                    break;
            }
        }
    }
}
