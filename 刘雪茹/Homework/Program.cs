﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Homework
{
    class Program
    {
        static void Main(string[] args)
        {
            //常量命名
            const string name = "达拉崩吧斑得贝迪卜多比鲁翁";
            const string skill = "骑马打怪兽";
            const string duty = "守护王国";
            Console.WriteLine("我的名字叫：{0}，我的技能是：{1}，我的职责是：{2}", name, skill, duty);
            //变量命名
            string studentName = "达拉崩吧斑得贝迪卜多比鲁翁";
            Console.WriteLine(studentName);

            //if...else条件结构
            string day = "Sunday";
            if (day == "Monday")
            {
                Console.WriteLine("上班");
            }
            else if (day == "Wednesday")
            {
                Console.WriteLine("加班");
            }
            else if (day == "Friday")
            {
                Console.WriteLine("开会");
            }
            else
            {
                Console.WriteLine("放假");
            }

            //switch case语句
            Console.WriteLine("请输入得分：");
            int score = int.Parse(Console.ReadLine());
            if (score < 0 || score > 100)
            {
                score = 0;
            }
            switch (score / 10)
                {
                    case 10:                  
                    case 9:
                        Console.WriteLine("优秀");
                        break;
                    case 8:
                        Console.WriteLine("良好");
                        break;
                    case 7:
                    case 6:
                        Console.WriteLine("及格");
                        break;
                    default:
                        Console.WriteLine("不及格");
                        break;
                }
        }
    }
}
