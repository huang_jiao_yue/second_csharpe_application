﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Moremore
{
    class Program
    {
        static void Main(string[] args)
        {
            //变量例题
            short year;
            int born;
            double age;
            // 实际初始化 
            year = 2020;
            born = 2001;
            age = year - born;
            Console.WriteLine("今年是:{0}, 出生于:{1}, 年纪:{2}", year, born, age);
            Console.ReadLine();

            //常量例题
            const double pi = 3.1415926;
            double r = 12;
            Console.ReadLine();
            double areaCircle = pi * r * r;
            Console.WriteLine("圆的半径: {0}, 圆的面积: {1}", r, areaCircle);
            Console.ReadKey();

            //判断switch例题
            Console.WriteLine("请输入老胡的评级(A、B、C、D、E)：");
            string grade = Console.ReadLine(); 
            double dSalary = 20000;  
            switch (grade)
            {
                case "A": dSalary += 500;
                    Console.WriteLine("A级奖金为： " + dSalary + "美元");
                    break;
                case "B":
                    dSalary += 200;
                    Console.WriteLine("B级奖金为： " + dSalary + "美元");
                    break;
                case "C":
                    Console.WriteLine("C级奖金为： " + dSalary + "美元");
                    break;
                case "D":
                    dSalary -= 200; 
                    Console.WriteLine("D级奖金为： " + dSalary + "美元");
                    break;
                case "E":
                    dSalary -= 500;
                    Console.WriteLine("E级奖金为： "+dSalary + "美元");
                    break;
                default:
                    Console.WriteLine("你没有输入等级，所以奖金为： "+dSalary + "美元");
                    break;
            }

            //判断if ..else例题
            Console.Write("请输入你的名字(老胡 or 小胡)：");
            string str_userName = Console.ReadLine();
            if (str_userName == "老胡")
            {
                Console.WriteLine("原来是你！！原来是你！！！");
            }
            else if (str_userName == "小胡")
            {
                Console.WriteLine("小胡？怎么是小胡呢！！ ");
            }
            else
            {
                Console.WriteLine("麻烦你按规矩来老弟，你这样我很焦灼");
            }
        }
    }
}
