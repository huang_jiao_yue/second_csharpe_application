﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp2
{
    class Program
    {
        static void Main(string[] args)
        {
            //for循环   循环输出

            for (int i = 1; i <= 10; i++)
            {
                Console.WriteLine("老八秘制小汉堡");
            }



            //双重循环  打印三角形
            int s = 5;
            for (int i = 1; i <= s; i++)
            {
                for (int t = 1; t <= s - i; t++)
                {
                    Console.Write(" ");
                }
                for (int k = 1; k <= 2 * i - 1; k++)
                {
                    Console.Write("*");
                }

                Console.WriteLine();
            }
            //else if     输入一个人名，判断是否为帅哥
            Console.WriteLine("请输入一个人的名字,判断身份");
            String Name = Console.ReadLine();
            if (Name == "陈泳洲")
            {
                Console.WriteLine("高富帅");
            }
            else if (Name == "徐汉权" || Name == "宋友起" || Name == "叶子荣" || Name == "张吴昱")
            {
                Console.WriteLine("二五仔");
            }
            else
            {
                Console.WriteLine("普通人");
            }

            //switch  语句

            Console.WriteLine("请输入你修仙等级   1-10");
            string Results = Console.ReadLine();
            int Result = int.Parse(Results);
            switch (Result)
            {
                case 10:
                    Console.WriteLine("天帝");
                    break;
                case 9:
                    Console.WriteLine("大帝");
                    break;
                case 8:
                    Console.WriteLine("仙帝");
                    break;
                case 7:
                    Console.WriteLine("御虚境");
                    break;
                case 6:
                    Console.WriteLine("合道境");
                    break;
                case 5:
                    Console.WriteLine("化神境");
                    break;
                case 4:
                    Console.WriteLine("炼神境");
                    break;
                case 3:
                    Console.WriteLine("气玄境");
                    break;
                case 2:
                    Console.WriteLine("炼气境");
                    break;
                case 1:
                    Console.WriteLine("凝气镜");
                    break;
                default:
                    Console.WriteLine("别乱打");

                    break;
            }



            Console.ReadKey();
        }
    }
}
