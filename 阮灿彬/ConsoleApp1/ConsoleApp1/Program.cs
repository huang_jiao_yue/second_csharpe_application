﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    class Program
    {
        static void Main(string[] args)
        {
            const double pi = 3.14;
            int r = 4;
            Console.WriteLine("圆的面积是" + pi * r);


            //if else
            Console.WriteLine("请输入你的财富值（大于0）");
            int points = int.Parse(Console.ReadLine());
            //如果输入的积分小于0则将其设置为0
            if (points < 0)
            {
                Console.WriteLine("乞讨");
            }
            else if (points <= 5)
            {
                Console.WriteLine("豪");
            }
            else if (points <= 10)
            {
                Console.WriteLine("土豪");
            }
            else if (points <= 20)
            {
                Console.WriteLine("富甲一方");
            }
            else
            {
                Console.WriteLine("富可敌国");
            }

            //switch case
            Console.WriteLine("请输入你的武力值（0~100的整数）");
            int right = int.Parse(Console.ReadLine());
            if (right < 0)
            {
                right = 0;
            }
            if (right > 100)
            {
                right = 100;
            }
            switch (right / 10 )
            {
                case 10:
                    Console.WriteLine("无敌");
                    break;
                case 9:
                    Console.WriteLine("强者");
                    break;
                case 8:
                    Console.WriteLine("强");
                    break;
                case 7:
                case 6:
                    Console.WriteLine("凡");
                    break;
                default:
                    Console.WriteLine("凡人");
                    break;
            }
        }
    }
}
