﻿using System;
using System.Threading;

namespace Homework
{
    class Program
    {
        static void Main(string[] args)
        {
            const double PI = 3.14;
            int r = 6;
            Console.WriteLine("圆面积:"+ PI*r*r);

            /* if else 应用  */
           /* WorkHouse = 程序员工作时间 */
            int WorkHouse = 15;
            if (WorkHouse < 2)
            {
                Console.WriteLine("初级工程师");
            }
            else if (WorkHouse < 5)
            {
                Console.WriteLine("中级工程师");
            }
            else if (WorkHouse < 10)
            {
                Console.WriteLine("高级工程师");
            }
            else if (WorkHouse < 20)
            {
                Console.WriteLine("资深工程师/架构师");
            }
            else
            {
                Console.WriteLine("该退休了");
            }


            /*Switch Case*/
            int points = 70;
            switch (points)
            {
                case 100:
                case 90:
                    Console.WriteLine("优秀");
                    break;
                case 80:
                case 70:
                case 60:
                    Console.WriteLine("及格");
                    break;
                default:
                    Console.WriteLine("不及格");
                    break;
            }
        }
    }
}
