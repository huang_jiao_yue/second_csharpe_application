﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    class Program
    {
        static void Main(string[] args)
        {
            //常量

            const double PI = 3.141;
            const int Chang = 5;
            const int Kuan = 3;
            int r = 5;
            Console.WriteLine("圆的面积"+PI* r * r);
            Console.WriteLine("圆的周长"+2* PI *r);
            Console.WriteLine("长方形的面积"+Chang * Kuan);
            Console.WriteLine( "长方形的面积"+(Chang+Kuan)*2);



            //if else

            Console.WriteLine("请输入一个整数（判断它是偶数还是奇数）：");
            int num = int.Parse(Console.ReadLine());
            if(num % 2 != 0)
            {
                Console.WriteLine(num+"是奇数");
            }
            else
            {
                Console.WriteLine(num+"是偶数");
            }




            //if else if

            Console.WriteLine("请输入你的战斗力（整数）");
            int num1 = int.Parse(Console.ReadLine());
            if (num1 < 0)
            {
                num1 = 0;
            }
            if (num1 <= 500)
            {
                Console.WriteLine("你的战斗力为渣渣");
            }else if(num1 <= 10000){
                Console.WriteLine("厉害");
            }else if (num1 <= 100000)
            {
                Console.WriteLine("你的战斗力diao爆了");
            }
            else
            {
                Console.WriteLine("你简直太牛咯");
            }





            //switch case 

            Console.WriteLine("输入孩子的考试分数（0-100整数）");
            int point = int.Parse(Console.ReadLine());
            switch(point / 10)
            {
                case 10:
                    Console.WriteLine("满分优秀");
                    break;
                case 9:
                    Console.WriteLine("优秀");
                    break;
                case 8:
                    Console.WriteLine("良好");
                    break;
                case 7:
                    Console.WriteLine("及格");
                    break;
                case 6:
                    Console.WriteLine("及格");
                    break;
                default:
                    Console.WriteLine("不及格等死吧");
                    break;
            }


            //for循环
            //打印1-10
            
            for(int i = 0; i <=10; i++)
            {
                Console.WriteLine(i);
             
            }

            //打印1-10的和
            int num2 = 0;
            for(int i = 0; i <= 10; i++)
            {
                num2 += i;
            }
            Console.WriteLine("1-10的和为"+num2);
        }
    }
}
