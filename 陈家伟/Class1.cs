﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace 第二次作业
{
    class Class1
    {
        static void Main(string[] args)
        {

            int point = 80;

            switch (point / 10)
            {
                case 10:
                    Console.WriteLine("优秀");
                    break;
                case 9:
                    Console.WriteLine("较优秀");
                    break;
                case 8:
                    Console.WriteLine("良好");
                    break;
                case 7:
                    Console.WriteLine("中等");
                    break;
                case 6:
                    Console.WriteLine("及格");
                    break;
                case 5:
                    Console.WriteLine("不及格");
                    break;
                case 4:
                    Console.WriteLine("不及格");
                    break;
                case 3:
                    Console.WriteLine("不及格");
                    break;
                case 2:
                    Console.WriteLine("不及格");
                    break;
                case 1:
                    Console.WriteLine("不及格");
                    break;
                case 0:
                    Console.WriteLine("不及格");
                    break;

            }
        }
    }
}
