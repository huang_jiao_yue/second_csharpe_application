﻿using System;

namespace myWork3
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("请输入学生考试的成绩（0~100的整数）");
            int points = int.Parse(Console.ReadLine());
            if (points < 0 || points > 100)
            {
                points = 0;
            }
            switch (points / 10)
            {
                case 10:
                case 9:
                    Console.WriteLine("优秀");
                    break;
                case 8:
                    Console.WriteLine("良好");
                    break;
                case 7:
                case 6:
                    Console.WriteLine("及格");
                    break;
                default:
                    Console.WriteLine("不及格");
                    break;
            }
        }
    }
}
