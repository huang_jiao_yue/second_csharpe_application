﻿using System;

namespace 王霁阳
{
    class Program
    {
        static void Main(string[] args)
        {
            //变量与常量
            int variable = 78;
            const int CONSTANT = 99;

            //if条件判断
            Console.WriteLine("请输入分数");
            int grade = int.Parse(Console.ReadLine());
            if (grade == 100)
            {
                Console.WriteLine("恭喜你满分");
            }
            else if(grade >=90)
            {
                Console.WriteLine("优秀");
            }
            else if(grade>=80)
            {
                Console.WriteLine("良好");
            }
            else if(grade>=60)
            {
                Console.WriteLine("及格");
            }
            else
            {
                Console.WriteLine("回家睡觉吧");
            }

            //switch  语句
            Console.WriteLine("请输入你的年龄");
            int yourAge = int.Parse(Console.ReadLine());
            yourAge = yourAge / 10;
            switch (yourAge)
            {
                case 15:
                case 14:
                case 13:
                case 12:
                case 11:
                case 10:
                    Console.WriteLine("福如东海，寿比南山");
                break;
                case 9:
                case 8:
                case 7:
                case 6:
                case 5:
                    Console.WriteLine("高寿");
                    break;
                default:
                    Console.WriteLine("漫漫人生路");
                    break;

            }



        }
    }
    //命名规范

    class HomePageGame   //类的命名方法
    {
 
    }
}